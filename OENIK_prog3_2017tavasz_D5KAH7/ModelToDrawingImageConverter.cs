﻿// <copyright file="ModelToDrawingImageConverter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OENIK_prog3_2017tavasz_D5KAH7
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Data;
    using System.Windows.Media;
    using System.Windows.Shapes;

    /// <summary>
    /// Az alakzatokat képpé konvertálja.
    /// </summary>
    public class ModelToDrawingImageConverter : IValueConverter
    {
        /// <summary>
        /// konvertáló metódus
        /// </summary>
        /// <param name="value"> view model példány</param>
        /// <param name="targetType"> --</param>
        /// <param name="parameter"> ---</param>
        /// <param name="culture"> ----</param>
        /// <returns>a kész képet</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            ViewModel model = (ViewModel)value;
            if (model == null)
            {
                return null;
            }

            PointToGeometryConverter posConv = new PointToGeometryConverter();
            PassiveObjectsToGeometryConverter wallConv = new PassiveObjectsToGeometryConverter();
            PassiveObjectsToGeometryConverter coinConv = new PassiveObjectsToGeometryConverter();

            Geometry wallGeo = (Geometry)wallConv.Convert(model.Walls, null, null, null);
            Geometry coinGeo = (Geometry)coinConv.Convert(model.Coins, null, null, null);
            Geometry playerGeo = (Geometry)posConv.Convert(model.Player, null, null, null);
            Geometry fruitGeo = (Geometry)posConv.Convert(model.Fruit, null, null, null);

            DrawingGroup dg = new DrawingGroup();

            dg.Children.Add(new GeometryDrawing(model.WallBrush, null, wallGeo));
            dg.Children.Add(new GeometryDrawing(model.CoinBrush, null, coinGeo));
            dg.Children.Add(new GeometryDrawing(model.FruitBrush, null, fruitGeo));

            foreach (Ghost item in model.Ghosts)
            {
                Geometry ghostGeo = (Geometry)posConv.Convert(item.Location, null, null, null);

                switch (item.Type)
                {
                    case MovementType.Horizontal:
                        dg.Children.Add(new GeometryDrawing(model.BlueGhostBrush, null, ghostGeo));
                        break;
                    case MovementType.Vertical:
                        dg.Children.Add(new GeometryDrawing(model.GreenGhostBrush, null, ghostGeo));
                        break;
                    case MovementType.Random:
                        dg.Children.Add(new GeometryDrawing(model.RedGhostBrush, null, ghostGeo));
                        break;
                    default:
                        break;
                }
            }

            dg.Children.Add(new GeometryDrawing(model.PlayerBrush, null, playerGeo));

            DrawingImage img = new DrawingImage();
            img.Drawing = dg;
            return img;
        }

        /// <summary>
        /// nem használjuk
        /// </summary>
        /// <param name="value"> --</param>
        /// <param name="targetType">---</param>
        /// <param name="parameter">----</param>
        /// <param name="culture">-----</param>
        /// <returns>-</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
