﻿// <copyright file="PassiveObjectsToGeometryConverter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace OENIK_prog3_2017tavasz_D5KAH7
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Data;
    using System.Windows.Media;

    /// <summary>
    /// Coin és wall átalakitása alakzattá.
    /// </summary>
    public class PassiveObjectsToGeometryConverter : IValueConverter
    {
        /// <summary>
        /// a konvertáló metódus
        /// </summary>
        /// <param name="value"> 2 dimenziós tömb </param>
        /// <param name="targetType">--</param>
        /// <param name="parameter"> ---</param>
        /// <param name="culture"> ----</param>
        /// <returns> geometrygroup </returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            GeometryGroup level = new GeometryGroup();
            bool[,] objects = (bool[,])value;
            for (int x = 0; x < objects.GetLength(0); x++)
            {
                for (int y = 0; y < objects.GetLength(1); y++)
                {
                    if (objects[x, y])
                    {
                        RectangleGeometry rg =
                            new RectangleGeometry();
                        rg.Rect = new System.Windows.Rect(
                                x * ViewModel.TILEW,
                                y * ViewModel.TILEH,
                                ViewModel.TILEW,
                                ViewModel.TILEH);
                        level.Children.Add(rg);
                    }
                }
            }

            return level;
        }

        /// <summary>
        /// nem hasznljuk
        /// </summary>
        /// <param name="value">-</param>
        /// <param name="targetType">--</param>
        /// <param name="parameter">----</param>
        /// <param name="culture">---</param>
        /// <returns>------</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
