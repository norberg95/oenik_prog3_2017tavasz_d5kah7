﻿// <copyright file="PointToGeometryConverter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace OENIK_prog3_2017tavasz_D5KAH7
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Data;
    using System.Windows.Media;

    /// <summary>
    /// Pontok alakzattá alakitása
    /// </summary>
    public class PointToGeometryConverter : IValueConverter
    {
        /// <summary>
        /// a konvertáló metódus
        /// </summary>
        /// <param name="value">pont</param>
        /// <param name="targetType">-</param>
        /// <param name="parameter">--</param>
        /// <param name="culture">---</param>
        /// <returns>----</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Point p = (Point)value;
            return new RectangleGeometry(new Rect(p.X * ViewModel.TILEW, p.Y * ViewModel.TILEH, ViewModel.TILEW, ViewModel.TILEH));
        }

        /// <summary>
        /// nem használjuk
        /// </summary>
        /// <param name="value">-</param>
        /// <param name="targetType">---</param>
        /// <param name="parameter">--</param>
        /// <param name="culture">----</param>
        /// <returns>-------</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

