﻿// <copyright file="BusinessLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OENIK_prog3_2017tavasz_D5KAH7
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Data;
    using System.Windows.Threading;

    /// <summary>
    /// Üzleti logika
    /// </summary>
    public class BusinessLogic
    {
        private Random r;
        private DispatcherTimer t;
        private int coinCounter;
        private bool lost = false;
        private int xsize;
        private int ysize;

        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessLogic"/> class.
        /// Fájl beolvasás, timer beállitások.
        /// </summary>
        /// <param name="mapfile"> fájlnév </param>
        public BusinessLogic(string mapfile)
        {
            // egy térképfájlból -> játékmenet
            string[] lines = File.ReadAllLines(mapfile);
            this.VM = new ViewModel();
            this.xsize = int.Parse(lines[0]);
            this.ysize = int.Parse(lines[1]);
            this.VM.Walls = new bool[this.xsize, this.ysize];
            this.VM.Coins = new bool[this.xsize, this.ysize];
            this.t = new DispatcherTimer();
            this.coinCounter = 0;

            this.t.Interval = TimeSpan.FromMilliseconds(300);
            this.t.Tick += this.T_Tick;
            this.t.Start();
            this.r = new Random();

            for (int x = 0; x < this.xsize; x++)
            {
                for (int y = 0; y < this.ysize; y++)
                {
                    // fal felismerése
                    if (lines[y + 2][x] == 'w')
                    {
                        this.VM.Walls[x, y] = true;
                    }
                    else
                    {
                        this.VM.Walls[x, y] = false;
                    }

                    // coin felismerésem coincounter feltöltése
                    if (lines[y + 2][x] == 'c')
                    {
                        this.VM.Coins[x, y] = true;
                        this.coinCounter++;
                    }
                    else
                    {
                        this.VM.Coins[x, y] = false;
                    }

                    // játékos felismerése
                    if (lines[y + 2][x] == 'P')
                    {
                        this.VM.Player = new Point(x, y);
                    }

                    // piros szellem
                    else if (lines[y + 2][x] == 'R')
                    {
                        this.VM.Ghosts.Add(new Ghost(new Point(x, y), MovementType.Random));
                    }

                    // zöld szellem
                    else if (lines[y + 2][x] == 'G')
                    {
                        this.VM.Ghosts.Add(new Ghost(new Point(x, y), MovementType.Vertical));
                    }

                    // kék szellem
                    else if (lines[y + 2][x] == 'B')
                    {
                        this.VM.Ghosts.Add(new Ghost(new Point(x, y), MovementType.Horizontal));
                    }

                    // gyümölcs
                    else if (lines[y + 2][x] == 'F')
                    {
                        this.VM.Fruit = new Point(x, y);
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets viewModel property
        /// </summary>
        public ViewModel VM { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether vesztettünk e.
        /// </summary>
        public bool Lost
        {
            get
            {
                return this.lost;
            }

            set
            {
                this.lost = value;
            }
        }

        /// <summary>
        /// Pacman lépéseit bonyilitja le, illetve az ezzel felmerülő eseményeket.
        /// </summary>
        /// <param name="dx">x kordináta változása</param>
        /// <param name="dy">y kordináta változása</param>
        /// <returns> Megnyertük e a játékot.</returns>
        public bool Move(int dx, int dy)
        {
            // hova kerülne a játékos
            // a lépés után
            int newx = (int)this.VM.Player.X + dx;
            int newy = (int)this.VM.Player.Y + dy;

            // Coin megszerzése
            if (this.VM.Coins[(int)this.VM.Player.X, (int)this.VM.Player.Y] == true)
            {
                this.VM.Coins[(int)this.VM.Player.X, (int)this.VM.Player.Y] = false;
                this.coinCounter--;
            }

            // fruit megszerzése, az ezzel járó szellem lassitás és az új fuit  random lerakása
            if (this.VM.Fruit.X == this.VM.Player.X && this.VM.Player.Y == this.VM.Fruit.Y)
            {
                int x;
                int y;
                do
                {
                    x = this.r.Next(1, this.xsize);
                    y = this.r.Next(1, this.ysize);
                }
                while (this.VM.Walls[x, y] == true);
                this.VM.Fruit = new Point(x, y);
                this.t.Interval = TimeSpan.FromMilliseconds(this.t.Interval.Milliseconds * 1.2);
            }

            // ha nincs több érme akkor nyertünk
            if (this.coinCounter == 0)
            {
                return true;
            }

            // ha valamelyik ghostal ütközünk akkor vesztettünk
            foreach (Ghost item in this.VM.Ghosts)
            {
                if (item.Location.X == this.VM.Player.X && item.Location.Y == this.VM.Player.Y)
                {
                    this.Lost = true;
                }
            }

            if (this.VM.Walls[newx, newy] == false)
            {
                // ha nincs fal, akkor mehet oda
                this.VM.Player = new Point(newx, newy);
            }

            return false;
        }

        private void T_Tick(object sender, EventArgs e)
        {
            // mindegyik ghost lép egyet
            foreach (Ghost item in this.VM.Ghosts)
            {
                if (item.Location.X == this.VM.Player.X && item.Location.Y == this.VM.Player.Y)
                {
                    this.Lost = true;
                }

                item.Move(this.VM.Walls);
            }

            this.VM.Refresh = new Point(1, 1);
        }
    }
}