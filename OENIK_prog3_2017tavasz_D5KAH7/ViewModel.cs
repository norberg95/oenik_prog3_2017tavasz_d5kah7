﻿// <copyright file="ViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OENIK_prog3_2017tavasz_D5KAH7
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Data;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Threading;

    /// <summary>
    /// viewmodel
    /// </summary>
    public class ViewModel : Bindable
    {
        /// <summary>
        /// egy egység szélessége
        /// </summary>
        public const int TILEW = 40;

        /// <summary>
        /// egy egység magassága
        /// </summary>
        public const int TILEH = 40;
        private bool[,] walls;
        private bool[,] coins;

        // point -> koordináta
        private ObservableCollection<Ghost> ghosts;

        private Point refresh;
        private Point fruit;
        private Point player;

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModel"/> class.
        /// </summary>
        public ViewModel()
        {
            this.ghosts = new ObservableCollection<Ghost>();
        }

        /// <summary>
        /// Gets pacman kép
        /// </summary>
        public Brush PlayerBrush
        {
            get { return this.GetBrush("pacman.bmp"); }
        }

        /// <summary>
        /// Gets coin kép
        /// </summary>
        public Brush CoinBrush
        {
            get { return this.GetBrush("coin.bmp"); }
        }

        /// <summary>
        /// Gets redghost kép
        /// </summary>
        public Brush RedGhostBrush
        {
            get { return this.GetBrush("redghost.bmp"); }
        }

        /// <summary>
        /// Gets greenghost kép
        /// </summary>
        public Brush GreenGhostBrush
        {
            get { return this.GetBrush("greenghost.bmp"); }
        }

        /// <summary>
        /// Gets blueghost kép
        /// </summary>
        public Brush BlueGhostBrush
        {
            get { return this.GetBrush("blueghost.bmp"); }
        }

        /// <summary>
        /// Gets wall kép
        /// </summary>
        public Brush WallBrush
        {
            get { return this.GetBrush("wall.bmp"); }
        }

        /// <summary>
        /// Gets fruit kép
        /// </summary>
        public Brush FruitBrush
        {
            get { return this.GetBrush("cherry.bmp"); }
        }

        /// <summary>
        /// Gets or sets szellem collection
        /// </summary>
        public ObservableCollection<Ghost> Ghosts
        {
            get
            {
                return this.ghosts;
            }

            set
            {
                this.ghosts = value;
                this.OnPropertyChanged("Self");
            }
        }

        /// <summary>
        /// Gets or sets ujratöltés
        /// </summary>
        public Point Refresh
        {
            get
            {
                return this.refresh;
            }

            set
            {
                this.refresh = value;
                this.OnPropertyChanged("Self");
            }
        }

        /// <summary>
        /// Gets ezt a példányt
        /// </summary>
        public ViewModel Self
        {
            get
            {
                return this;
            }
        }

        /// <summary>
        /// Gets or sets walls
        /// </summary>
        public bool[,] Walls
        {
            get
            {
                return this.walls;
            }

            set
            {
                this.walls = value;
                this.OnPropertyChanged("Self");
            }
        }

        /// <summary>
        /// Gets or sets pacman
        /// </summary>
        public Point Player
        {
            get
            {
                return this.player;
            }

            set
            {
                this.player = value;
                this.OnPropertyChanged("Self");
            }
        }

        /// <summary>
        /// Gets or sets coin
        /// </summary>
        public bool[,] Coins
        {
            get
            {
                return this.coins;
            }

            set
            {
                this.coins = value;
                this.OnPropertyChanged("Self");
            }
        }

        /// <summary>
        /// Gets or sets fruit
        /// </summary>
        public Point Fruit
        {
            get
            {
                return this.fruit;
            }

            set
            {
                this.fruit = value;
                this.OnPropertyChanged("Self");
            }
        }

        /// <summary>
        /// képből brush
        /// </summary>
        /// <param name="imagefile"> képfájl </param>
        /// <returns> brush </returns>
        private Brush GetBrush(string imagefile)
        {
            // tetszőleges képből -> ecset
            ImageBrush ib = new ImageBrush(new BitmapImage(new Uri(imagefile, UriKind.Relative)));
            ib.TileMode = TileMode.Tile;
            ib.Viewport = new Rect(0, 0, TILEW, TILEH);
            ib.ViewportUnits = BrushMappingMode.Absolute;
            return ib;
        }
    }
}
