﻿// <copyright file="MainWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace OENIK_prog3_2017tavasz_D5KAH7
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Navigation;
    using System.Windows.Shapes;

    // PACMAN

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private BusinessLogic bL;
        private int levelnumber;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// Window konstruktor
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
            this.levelnumber = 1;
            this.bL = new BusinessLogic("L0" + this.levelnumber + ".lvl");
            this.DataContext = this.bL.VM;
            this.KeyDown += this.MainWindow_KeyDown;
        }

        private void MainWindow_KeyDown(object sender, KeyEventArgs e)
        {
            bool isOver = false;

            // pacman mozgása
            switch (e.Key)
            {
                case Key.Left:
                    isOver = this.bL.Move(-1, 0); break;
                case Key.Right:
                    isOver = this.bL.Move(1, 0); break;
                case Key.Up:
                    isOver = this.bL.Move(0, -1); break;
                case Key.Down:
                    isOver = this.bL.Move(0, 1); break;
            }

            // ha vesztettél
            if (this.bL.Lost == true)
            {
                MessageBox.Show("Vesztettél");
                this.Close();
            }

            // ha teljesitetted a pályát
            if (isOver)
            {
                MessageBox.Show("Nyertél!");
                if (levelnumber<5)
                {
                    this.levelnumber++;

                    this.bL = new BusinessLogic("L0" + this.levelnumber + ".lvl");
                    this.DataContext = this.bL.VM;
                }
                else
                {
                    this.Close();
                }
               
            }
        }
    }
}
