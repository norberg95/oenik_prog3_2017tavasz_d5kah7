﻿// <copyright file="Bindable.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OENIK_prog3_2017tavasz_D5KAH7
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Adatkötés
    /// </summary>
    public class Bindable : INotifyPropertyChanged
    {
        /// <summary>
        /// változást észlelő esemény
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// változáskor meghivható metódus
        /// </summary>
        /// <param name="name"> hivó neve </param>
        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
