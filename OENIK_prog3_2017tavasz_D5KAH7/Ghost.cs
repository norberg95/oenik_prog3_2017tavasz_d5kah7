﻿// <copyright file="Ghost.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OENIK_prog3_2017tavasz_D5KAH7
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// Háromféle szellemmozgást különböztetünk meg: statikus vertikális, statikus horizontális és random.
    /// </summary>
    public enum MovementType
    {
        /// <summary>
        /// horizontális mozgás
        /// </summary>
        Horizontal,

        /// <summary>
        /// vertikális mozgás
        /// </summary>
        Vertical,

        /// <summary>
        /// random mozgás
        /// </summary>
        Random
    }

    /// <summary>
    /// A szellemek
    /// </summary>
    public class Ghost : Bindable
    {
        private Point location;
        private MovementType type;
        private int direction;
        private Random r;

        /// <summary>
        /// InitializPes a new instance of the <see cref="Ghost"/> class.
        /// </summary>
        /// <param name="location"> helyzet </param>
        /// <param name="type"> mozgási tipus </param>
        public Ghost(Point location, MovementType type)
        {
            this.location = location;
            this.type = type;
            this.direction = 1;
            this.r = new Random();
        }

        /// <summary>
        /// Gets or sets helyzet
        /// </summary>
        public Point Location
        {
            get
            {
                return this.location;
            }

            set
            {
                this.location = value;
            }
        }

        /// <summary>
        /// Gets or sets irány
        /// </summary>
        public int Direction
        {
            get
            {
                return this.direction;
            }

            set
            {
                this.direction = value;
            }
        }

        /// <summary>
        /// Gets or sets mozgási tipus
        /// </summary>
        internal MovementType Type
        {
            get
            {
                return this.type;
            }

            set
            {
                this.type = value;
            }
        }

        /// <summary>
        /// szellem mozgások
        /// </summary>
        /// <param name="walls"> fal tömb, hogy tudjuk, hova nem léphet</param>
        public void Move(bool[,] walls)
        {
            // random mozgás
            if (this.type == MovementType.Random)
            {
                int randomirany = this.r.Next(1, 5);

                switch (randomirany)
                {
                    case 1:
                        if (walls[(int)this.location.X, (int)this.location.Y + 1] == false)
                        {
                            this.location = new Point(this.location.X, this.location.Y + 1);
                        }

                        break;
                    case 2:
                        if (walls[(int)this.location.X, (int)this.location.Y - 1] == false)
                        {
                            this.location = new Point(this.location.X, this.location.Y - 1);
                        }

                        break;
                    case 3:
                        if (walls[(int)this.location.X + 1, (int)this.location.Y] == false)
                        {
                            this.location = new Point(this.location.X + 1, this.location.Y);
                        }

                        break;
                    case 4:
                        if (walls[(int)this.location.X - 1, (int)this.location.Y] == false)
                        {
                            this.location = new Point(this.location.X - 1, this.location.Y);
                        }

                        break;
                }
            }

            // horizontális mozgás
            else if (this.type == MovementType.Horizontal)
            {
                if (walls[(int)this.location.X, (int)this.location.Y + this.direction] == false)
                {
                    this.location = new Point(this.location.X, this.location.Y + this.direction);
                }
                else
                {
                    this.direction = this.direction * -1;
                }
            }

            // vertikális mozgás
            else if (this.type == MovementType.Vertical)
            {
                if (walls[(int)this.location.X + this.direction, (int)this.location.Y] == false)
                {
                    this.location = new Point(this.location.X + this.direction, this.location.Y);
                }
                else
                {
                    this.direction = this.direction * -1;
                }
            }
        }
    }
}
